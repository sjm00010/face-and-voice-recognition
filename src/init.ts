// import nodejs bindings to native tensorflow,
// not required, but will speed up things drastically (python required)
import '@tensorflow/tfjs-node';

// implements nodejs wrappers for HTMLCanvasElement, HTMLImageElement, ImageData
import * as canvas from 'canvas';
import * as faceapi from '@vladmandic/face-api';

export const setupEnv = () => {
    // patch nodejs environment, we need to provide an implementation of
    // HTMLCanvasElement and HTMLImageElement
    const { Canvas, Image, ImageData }: { Canvas: any, Image: any, ImageData: any } = canvas;
    faceapi.env.monkeyPatch({ Canvas, Image, ImageData });
};


export const loadModels = async (path: string): Promise<void> => {
    return Promise.all([
        faceapi.nets.ssdMobilenetv1.loadFromDisk(path),
        faceapi.nets.faceRecognitionNet.loadFromDisk(path),
        faceapi.nets.faceLandmark68Net.loadFromDisk(path),
    ]).then()
};

export const modelsPath = './models';
export const imagesPath = './data2';
export const audiosPath = './dataAudios';
