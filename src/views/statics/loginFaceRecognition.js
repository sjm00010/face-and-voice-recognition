const video = document.getElementById('videoSource');
const canvas = document.getElementById('imageBuffer');


async function loginUser(bufferData) {
    const dataToSend = {
        data: bufferData
    };
    return await fetch('/faceLogin', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(dataToSend)
    });
}

startCamera(video, canvas);

document.getElementById('initCamera').addEventListener('click', () => startCamera(video, canvas));
document.getElementById('stopCamera').addEventListener('click', () => stopCamera(video));

document.getElementById('detectUser').addEventListener('click', () => {
    document.getElementById('detectUser').disabled = true;
    const buffer = getVideoBuffer(video, canvas);
    loginUser(buffer)
        .then(response => {
            if (response.status == 401) {
		displayMessage('Unkown user');
		clearMessage();
            }

            if (response.redirected) {
                window.location.href = response.url;
            }
        }).catch(error => {
            console.error('Error detecting user: ' + error);
        }).finally(() => {
            document.getElementById('detectUser').disabled = false;
        });
});
