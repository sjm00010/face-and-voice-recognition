const video = document.getElementById('videoSource');
const canvas = document.getElementById('imageBuffer');
startCamera(video, canvas);

const { recognition, recorder, audioAnalyser } = await startSpeech({
    onStart: handleStart,
    onResult: handleDetection,
    onData: handleRecordingData,
    onStop: handleStop,
    onError: handleError,
});

let recordingChunks = [];
let recordedFrequency = [];

function sendRegisterRequest(dataToSend){
    return fetch('/register', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(dataToSend)
    });
}

function handleStart(){
    recordingChunks = [];
    recordedFrequency = [];

    displayMessage('Listening!!\nSpeak your username now!!');
}

function handleDetection({results}){
    const usernameInput = document.getElementById('usernameInput');

    for(let i=0;i < results.length; i++){
	const result = results[i];
	if(result.isFinal){
	    const best = getBestResultAlternative(result);
	    usernameInput.value = best.transcript;
	}
    }
}

function handleRecordingData({data}){
    recordingChunks.push(data);

    const dataArray = new Uint8Array(audioAnalyser.frequencyBinCount);
    audioAnalyser.getByteFrequencyData(dataArray);
    recordedFrequency.push(...dataArray.filter(data => data != 0));
    document.getElementById('totalFrequencySamples').innerHTML = recordedFrequency.length;
}

function handleStop(){
    clearMessage({delay: 0});
    const audioElement = document.getElementById('audioSample');
    const blob = getAudioBlob(recordingChunks);
    const audioURL = window.URL.createObjectURL(blob);

    audioElement.src = audioURL;
    audioElement.controls = true;
    document.getElementById('totalFrequencySamples').innerHTML = recordedFrequency.length;
}

function handleError({error, message}){
    if(error == "no-speech"){
	displayMessage('No speech detected, stoping detection');
	clearMessage({delay: 5000});
    }
}

document.getElementById('initCamera').addEventListener('click', () => startCamera(video, canvas));
document.getElementById('stopCamera').addEventListener('click', () => stopCamera(video));
document.getElementById('recordButton').addEventListener('mousedown', () => startListening({ recognition, recorder }))
;document.getElementById('recordButton').addEventListener('mouseup', () => stopListening({ recognition, recorder }));
document.getElementById('registerButton').addEventListener('click',async () => {
    document.getElementById('registerButton').disabled = true;
    const videoBuffer = getVideoBuffer(video, canvas);
    const username = document.getElementById('usernameInput').value;
    const audioFrequencies = recordedFrequency;

    if(audioFrequencies.length < 120){
	displayMessage('Not enough datasamples please continue speaking');
	clearMessage({delay: 5000});
    }
    
    sendRegisterRequest({
	videoBuffer,
	username,
	audioFrequencies,
    }).then(async response => {
	if(response.status == 201){
	    displayMessage('Successfully created user as '+ username);
	    clearMessage({delay: 5000});
	    return;
	}

	return await response.json();
    }).then(({errorMsg}) =>{
	if(errorMsg){
	    displayMessage(errorMsg);
	    clearMessage();
	}
    }).catch(error => {
	console.error('Error creating a new user: ' + error);
    }).finally(()=>{
	document.getElementById('registerButton').disabled = false;
    });
});
