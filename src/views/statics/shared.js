
async function startSpeech({
    onStart = () => {},
    onResult= () => {},
    onData  = () => {},
    onStop  = () => {},
    onError = () => {},
} = {}){
    if(!('webkitSpeechRecognition' in window))
	throw new Error('Speech recognition not supported in this browser')

    return await navigator.mediaDevices.getUserMedia({
	audio: true
    }).then(stream => {
	const recognition = new webkitSpeechRecognition();
	const recorder = new MediaRecorder(stream, {
	    mimeType: 'audio/webm;codecs=opus'
	});
	const audioCtx = new AudioContext();
	const source = audioCtx.createMediaStreamSource(stream);
	const audioAnalyser = audioCtx.createAnalyser();
	source.connect(audioAnalyser);

	recognition.continuous = true;
	recognition.interimResults = true;
	//recognition.lang = 'en-UK';
	recognition.lang = 'es-ES';
	
	recognition.onstart = onStart;
	recognition.onresult = onResult;
	recognition.onend = onStop;
	recognition.onerror = onError;

	recorder.ondataavailable = onData;

	return {
	    recognition,
	    recorder,
	    audioAnalyser,
	}
    });
}

async function startCamera(video, canvas) {
    return await navigator.mediaDevices.getUserMedia({
        video: true
    }).then(stream => {
        video.srcObject = stream;
        return video.play();
    }).then(() => {
        video.setAttribute('width', video.videoWidth);
        video.setAttribute('height', video.videoHeight);

        canvas.width = video.videoWidth;
        canvas.height = video.videoHeight;
    }).catch(error => {
        console.error('Error starting the camera: ' + error);
    });
}

function stopCamera(video) {
    const stream = video.srcObject;
    stream.getTracks().forEach(function (track) {
        track.stop();
    });
}

function getVideoBuffer(video, canvas) {
    const context = canvas.getContext('2d');
    context.drawImage(video, 0, 0);
    const data = canvas.toDataURL('image/jpeg');
    return data;
}


function clearMessage({delay = 2000, element = document.getElementById('errorMsg')} = {}){
    setTimeout(() => element.innerHTML = '', delay);
}

function displayMessage(message, element = document.getElementById('errorMsg')){
    element.innerHTML = message;
}

function startListening({ recognition, recorder }){
    recognition.start();
    recorder.start();
}

function stopListening({ recognition, recorder }){
    recognition.stop();
    recorder.stop();
}

function getBestResultAlternative(result){
    const best = result[0];

    for(let i=0;i < result.length; i++)
	if(best.confidence < result[i].confidence)
	    best = result[i];
    
    return best;
}

function getAudioBlob(audioChunks){
    return new Blob(audioChunks, { type: 'audio/webm;codecs=opus' });
}

function getAudioBuffer(audioBlob){
    return new Promise((resolve, reject) => {
	const reader = new FileReader();
	reader.onload = () => resolve(reader.result);
	reader.onerror = reject;

	reader.readAsDataURL(audioBlob);
    });
}
