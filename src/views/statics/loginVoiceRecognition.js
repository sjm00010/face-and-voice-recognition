
let recordingChunks = [];
let recordedFrequency = [];
const { recognition, recorder, audioAnalyser } = await startSpeech({
    onStart: handleStart,
    onResult: handleDetection,
    onData: handleRecordingData,
    onStop: handleStop,
    onError: handleError,
});
 
function sendVoiceLoginRequest(dataToSend){
    return fetch('/voiceLogin', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(dataToSend)
    })
}

function handleStart(){
    recordingChunks = [];
    recordedFrequency = [];
    
    displayMessage('Listening!!\nSpeak your username now!!');
}

function handleDetection({results}){
    const usernameInput = document.getElementById('usernameInput');

    for(let i=0;i < results.length; i++){
	const result = results[i];
	if(result.isFinal){
	    const best = getBestResultAlternative(result);
	    usernameInput.value = best.transcript;
	}
    }
}

function handleRecordingData({data}){
    recordingChunks.push(data);

    const dataArray = new Uint8Array(audioAnalyser.frequencyBinCount);
    audioAnalyser.getByteFrequencyData(dataArray);
    recordedFrequency.push(...dataArray.filter(data => data != 0));
    document.getElementById('totalFrequencySamples').innerHTML = recordedFrequency.length;
}

function handleStop(){
    clearMessage({delay: 0});
    const audioElement = document.getElementById('audioSample');
    const blob = getAudioBlob(recordingChunks);
    const audioURL = window.URL.createObjectURL(blob);

    audioElement.src = audioURL;
    audioElement.controls = true;

    document.getElementById('totalFrequencySamples').innerHTML = recordedFrequency.length;
}

function handleError({error, message}){
    if(error == "no-speech"){
	displayMessage('No speech detected, stoping detection');
	clearMessage({delay: 5000});
    }
}

document.getElementById('recordButton').addEventListener('mousedown', () => startListening({ recognition, recorder }))
;document.getElementById('recordButton').addEventListener('mouseup', () => stopListening({ recognition, recorder }));

document.getElementById('detectUser').addEventListener('click',async () => {
    document.getElementById('detectUser').disabled = true;
    const username = document.getElementById('usernameInput').value;
    const audioFrequencies = recordedFrequency;

    if(audioFrequencies.length < 60){
	displayMessage('Not enough datasamples please repeat');
	clearMessage({delay: 5000});
    }
    
    sendVoiceLoginRequest({
	username,
	audioFrequencies,
    }).then(response => {
	if (response.status == 401) {
	    displayMessage('Unkown user');
	    clearMessage();
	}

	if (response.status == 404) {
	    displayMessage(`User ${usename} not registered`);
	    clearMessage();
	}

        if (response.redirected) {
            window.location.href = response.url;
        }
    }).catch(error => {
        console.error('Error detecting user: ' + error);
    }).finally(() => {
        document.getElementById('detectUser').disabled = false;
    });
});



