import { format, dirname, extname, basename, join } from 'path';
import { promisify } from 'util';
import { rename, unlink, readdir, readFile } from 'fs/promises';

export async function getExistingAudioFrequencies(
    directory:string,
    extension: string = '.bin'):
Promise<number[][]>{
    return Promise.all(
	(await readdir(directory))
	    .filter(file => file.endsWith(extension))
	    .map(async freqFile => await readFile(join(directory, freqFile)))
	    .map(async fileData => Buffer.from(await fileData))
	    .map(async buffer => Array.from(new Uint8Array(await buffer)))
    );
}

export function compareAudioFrequencies(arr1: number[], arr2: number[], maxStdDeviation = 3): boolean {
    if(arr1.length < arr2.length)
	[arr1, arr2] = [arr2, arr1];

    const stats = getDistanceStatistics(arr1, arr2);

     console.log(stats);
    
    return stats.stdDeviation < maxStdDeviation;
}

interface genome{
    slice: number[],
    distance: number
}

function extractMinimalDistance(base: number[], section: number[]): number[]{
    return base.reduce((
	minSlice: genome,
	_: number,
	i: number
    ):genome => {
	if(base.length - i < section.length) return minSlice;
	
	const slice = base.slice(i, i + section.length);	
	const distance = meanAbsolutePercentageError(slice, section);
	
	if(distance < minSlice.distance) return { slice, distance };
	
	return minSlice;
    },{slice: [], distance: Infinity})
	.slice;
}

interface statisticsResult{
    minDistance: genome,
    maxDistance: genome,
    averageDistance: number,
    normalizedAverageDistance: number,
    stdDeviation: number,
    normalizedStdDeviation: number,
}

function getDistanceStatistics(base: number[], section: number[]):statisticsResult
{
    const genomeArr: genome[] = base
	.map((_: number, i: number): genome => {
	    if(base.length - i < section.length) return { slice: [], distance: NaN };
	    
	    const slice = base.slice(i, i + section.length);
	    const distance = meanAbsoluteError(slice, section);

	    return { slice, distance };
	})
	.filter(gen => !isNaN(gen.distance));
    
    return calculateStats(genomeArr);
}

function calculateStats(genomeArr: genome[]): statisticsResult{
    let sum = 0;
    let sumSquared = 0;
    let minGen: genome = { slice: [], distance: 1000};
    let maxGen: genome = { slice: [], distance: -1000};

    for(const gen of genomeArr){	
	sum += gen.distance;
	sumSquared += gen.distance ** 2;

	if(gen.distance > maxGen.distance)
	    maxGen = gen;
	
	if(gen.distance < minGen.distance)
	    minGen = gen;
    }

    const length = genomeArr.length;
    const mean = sum / length;
    const variance = (sumSquared / length) - (mean ** 2);
    const stdDev = Math.sqrt(variance);
    const range = maxGen.distance - minGen.distance;
    
    return {
	minDistance: minGen,
	maxDistance: maxGen,
	averageDistance: mean,
	normalizedAverageDistance: (mean - minGen.distance) / range,
	stdDeviation: stdDev,
	normalizedStdDeviation: stdDev / range,
    }
}

function meanSquaredError(arr1: number[], arr2: number[]): number{
    const sum = arr1.reduce((acc, cur, i) => acc + (cur - arr2[i]) ** 2, 0);
    return sum / arr1.length;
}

function meanAbsoluteError(arr1: number[], arr2: number[]): number{
    const sum = arr1.reduce((acc, cur, i) => acc + Math.abs(cur - arr2[i]), 0);
    return sum / arr1.length;
}

function meanAbsolutePercentageError(arr1: number[], arr2: number[]): number{
    const mae = meanAbsoluteError(arr1, arr2);
    const total = arr1.reduce((acc, cur) => acc + cur, 0) + arr2.reduce((acc, cur) => acc + cur, 0);
    const mean = total / (arr1.length + arr2.length);

    return (mae / mean) * 100;
}
