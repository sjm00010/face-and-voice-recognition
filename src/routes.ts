import express, { Request, Response, Router } from 'express';
import { writeFile } from 'fs/promises';
import { existsSync, mkdirSync, unlinkSync, writeFileSync } from 'fs';
import { faceMatcher, upadteMatcher, recognize, containsFace } from './recognition';
import { imagesPath, audiosPath } from './init';
import { join } from 'path';
import { getExistingAudioFrequencies, compareAudioFrequencies } from './helper';

declare module 'express-session' {
    export interface SessionData {
        username: string;
        errorMsg: string;
        loginMethod: string;
    }
}

const router: Router = express.Router();

interface voiceLoginBody{
    username: string,
    audioFrequencies: number[],
}

router.post('/voiceLogin', async (req: Request, res: Response) => {
    const { username, audioFrequencies }: voiceLoginBody = req.body;
    
    const audioPath = join(audiosPath, username);
    const frequencies = audioFrequencies.filter(freq => freq !== 0);
    
    if (!existsSync(audioPath)){
	res.status(404).send({ errorMsg: "No known username" });
        return;
    }

    const frequenciesToTest = await getExistingAudioFrequencies(audioPath);
    
    const authenticated = frequenciesToTest.some((freq: number[]) => compareAudioFrequencies(freq, frequencies));

    if(!authenticated){
	res.status(401).send({ errorMsg: "Unknown user" });
        return;
    }
    
    req.session.username = username;
    res.redirect(`welcome`);    
});

interface registerBody {
    videoBuffer: string | Buffer,
    username: string,
    audioFrequencies: number[]
}

router.post('/register', async (req: Request, res: Response) => {
    const { videoBuffer, username, audioFrequencies }: registerBody  = req.body;

    if(!(await containsFace(videoBuffer as Buffer))){
	res.status(404).send({ errorMsg: 'No face detected' });
	return;
    }

    if(username == ''){
	res.status(404).send({ errorMsg: "Must provide an username!!" });
        return;
    }

    const labelPath = join(imagesPath, username);
    const audioPath = join(audiosPath, username);
    const frequencies = Buffer.from(audioFrequencies.filter(f => f !== 0));
    const img = (videoBuffer as string).replace(/^data:image\/(png|jpeg|jpg);base64,/, '');
    const timestamp = Date.now();
    if (!existsSync(labelPath)) mkdirSync(labelPath);
    if (!existsSync(audioPath)) mkdirSync(audioPath);

    await writeFile(join(labelPath, `${timestamp}.jpg`), img, {
        encoding: 'base64'
    });
    
    await upadteMatcher();

    console.log('Ready to recognize faces');

    await writeFile(join(audioPath, `${timestamp}.bin`), frequencies, 'utf8');

    res.sendStatus(201);    
});

router.post("/faceLogin", async (req: Request, res: Response) => {
    const { data } = req.body;
    const img: Buffer = data.replace(/^data:image\/(png|jpeg|jpg);base64,/, '');
    const result = await recognize(data, faceMatcher);

    if (result.label.length == 0 || result.label == 'unknown') {
        res.status(401).send({ errorMsg: "No face detected" });        
        return;
    }

    req.session.username = result.label;
    res.redirect(`welcome`);
});

router.get("/", (req: Request, res: Response) => {    
    const msg = req.session.errorMsg ?? '';
    req.session.errorMsg = undefined;

    const loginView = req.query.loginMethod as string ?? req.session.loginMethod ?? 'faceRecognition';
    req.session.loginMethod = loginView;

    res.render(`views/login_${loginView}`, { errorMsg: msg});
});

router.get("/welcome", (req: Request, res: Response) => {
    if (req.session.username == undefined) {
        req.session.errorMsg = "You must login first";
        res.redirect('/');
        return;
    }

    res.render('views/welcome', { username: req.session.username });
});

router.post("/logout", (req: Request, res: Response) => {
    req.session.username = undefined;
    res.redirect('/');
});

router.get("/register", (req: Request, res: Response) => {
    const msg = req.session.errorMsg ?? '';
    req.session.errorMsg = undefined;

    res.render('views/register', { errorMsg: msg});
});


export default router;
