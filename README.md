<p align="center">
  <a href="" rel="noopener">
 <img width=200px height=200px src="https://i.imgur.com/FxL5qM0.jpg" alt="Bot logo"></a>
</p>

<h3 align="center">user-recognition</h3>

<div align="center">

[![License](https://img.shields.io/badge/license-GPL%203.0-blue.svg)](/LICENSE)

</div>

---

<p align="center"> 
Proof of concept to authenticate using facial or speaker recognition.
</p>

## 📝 Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Deployment](#deployment)
- [Usage](#usage)
- [Built Using](#built_using)
- [TODO](TODO.md)
- [Authors](#authors)

## 🧐 About <a name = "about"></a>
Proof of concept that launchs a server where you can login using facial recognition or spearker recognition.

So far the server reloads the face model each time a new user is created, and for each one data is stored locally.

## 🏁 Getting Started <a name = "getting_started"></a>

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See [deployment](#deployment) for notes on how to deploy the project on a live system.

### Prerequisites

Software requirements

- [NodeJS v16.x](https://nodejs.org/download/release/latest-v16.x)
- [PNPM v7.x](https://pnpm.io/installation)
- You must open the client in the latests version of chrome to use voice recognition

Hardware requirements:

- Minimun RAM: 450 MB
- Minimun space for:
  - deployment: 16 MB
  - development: 12 MB
  - development after installing dependencies: 1 GB


### Installing

After downloading the project you can install all the dependencies using:

```
pnpm install
```

Create a folder to store your data and inside that folder create another folder named random, inside place a image taken from [This person does not exist](https://thispersondoesnotexist.com/). This step is required since you need at least one face for the model.

Once that is done go to [src/init.ts](src/init.ts) and update the variables `audiosPath` ,`imagesPath` (and `modelsPath` if using another models for face detection) to match what you just created.


To start the server run:

```
pnpm start
```

In the case you need to change the port go to [src/index.ts](src/index.ts) and update it. Once the server is running you should be able to see the state of the application in the terminal.

By default the server is deployed at [http://localhost:3000](http://localhost:3000) the page requires javascript and cookies for session management so ensure both are enabled.

## 🎈 Usage <a name="usage"></a>

Once the server is deployed the user can now open it in the browser and sign up or sign in. If the user is already logged in the session is maintained as long as the cookies are valid.

In the `welcome` page the user can now see his username, this page is blocked without previous authentication, inside this page the user may now log out and return to the login page.

## 🚀 Deployment <a name = "deployment"></a>

After the first successful start of the application, the code should be compiled to Javascript. All you need to deploy are the folders `dist`, `models` and `data`.

So far the project can't generate a single bundle so until that is done you must also copy the `node_modules` folder to deploy.

## ⛏️ Built Using <a name = "built_using"></a>

- [Express](https://expressjs.com/) - Server Framework
- [NodeJs](https://nodejs.org/en/) - Server Environment

## ✍️ Authors <a name = "authors"></a>

- [flo00008@red.ujaen.es](mailto:flo00008@red.ujaen.es)
- [sjm00010@red.ujaen.es](mailto:sjm00010@red.ujaen.es)
